import { select as d3Select } from 'd3-selection'
import { axisBottom as d3AxisBottom } from 'd3-axis'
import { max as d3Max, histogram as d3Histogram } from 'd3-array'
import { scaleLinear as d3ScaleLinear } from 'd3-scale'
import { transition as d3Transition } from 'd3-transition'

let histogram = function(selector) {
  let m = {top:0,right:0,bottom:0,left:0}
  let w = 800, h = 400;
  let x, y, svg, xAxisG, yAxisG;

  let drawed = false;

  const margin = (top ,right, bottom, left) => {
    m = { top, right, bottom, left }
    return methods
  }

  const width = wi => {
    w = wi - m.left - m.right
    return methods
  }

  const height = he => {
    h = he - m.top - m.bottom
    return methods
  }

  const init = () => {
    x = d3ScaleLinear().range([0, w]);
    y = d3ScaleLinear().range([h, 0]);

    svg = d3Select(selector)
      .append("svg")
      .attr('width', w + m.left + m.right)
      .attr('height', h + m.top + m.bottom)
      .append('g')
      .attr('transform', `translate(${m.left},${m.top})`);

    xAxisG = svg.append("g")
      .attr("class", "x axis")
      .attr("transform", `translate(0,${h})`);

    return methods
  }

  const draw = data => {
    let hours = 24;

    x.domain([0, hours]);

    let bins = d3Histogram()
      .domain(x.domain())
      .thresholds(hours)
      (data);

    y.domain([0, d3Max(bins, d => d.length)]);

    xAxisG.call(d3AxisBottom(x).ticks(hours));

    if (!drawed) {
      let bar = svg.selectAll(".bar")
        .data(bins)
        .enter()
        .append("g")
        .attr("class", "bar")
        .attr("transform", d => "translate(" + x(d.x0) + "," + y(d.length) + ")");

      bar.append("rect")
        .attr("x", 1)
        .attr("width", x(1) - 1)
        .attr("height", d => h - y(d.length));

      bar.append('text')
        .attr('dy', '.75em')
        .attr('y', 6)
        .attr('x', (x(bins[0].x1) - x(bins[0].x0)) / 2)
        .attr('text-anchor', 'middle')
        .text(d => d.length)

      drawed = true;
    } else {
      let bar = svg.selectAll('.bar')
        .data(bins)
        .transition(d3Transition())

      bar.attr("transform", d => "translate(" + x(d.x0) + "," + y(d.length) + ")");

      bar.select('rect').attr("height", d => h - y(d.length));

      bar.select('text').text(d => d.length)
    }

    return methods
  }

  const methods = { init, draw, width, height, margin }

  return methods
}

export default histogram
