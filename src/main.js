import Rx from '@reactivex/rxjs'
import axios from 'axios'
import histogram from './chart/histogram'
import {sentry} from './config'
import Raven from 'raven-js'

// sends errors to sentry in production
if (process.env.NODE_ENV == 'production') {
  Raven.config(`https://${sentry.key}@sentry.io/${sentry.app}`).install()
}

let hist = histogram("div#graph")
  .margin(10, 30, 30, 30)
  .width(800)
  .height(400)
  .init()

let main = document.querySelector('main')
let titleScreenName = document.querySelector('#title_screen_name')

let form = document.querySelector('form')
let formSubmit = Rx.Observable.fromEvent(form, 'submit')
  .map(e => e.preventDefault())

let input = form.querySelector('input')
let inputInput = Rx.Observable.fromEvent(input, 'input')

let request = formSubmit
  .withLatestFrom(inputInput, (submit, input) => input.target.value)
  .filter(i => i.length > 0);

let response = request.flatMap(screen_name => Rx.Observable.fromPromise(axios.post('/tweets', {screen_name})))
  .map(data => data.data.map(t => new Date(t)))

response.subscribe(data => {
  titleScreenName.textContent = input.value
  main.style.display = ''

  let byHour = data.map(d => d.getHours())
  hist.draw(byHour)
});
