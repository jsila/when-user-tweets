const sinon = require('sinon')
const assert = require('assert')
const httpMocks = require('node-mocks-http')

let schema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "list": {
      "type": "array",
      "items": {
        "type": "string"
      }
    }
  },
  "required": ["list"]
}

let validateJSONBody = require('./validateJSONBody')

describe('validate', () => {
  it('calls next if json matches schema or returns 400 if not', () => {
    let testParams = [
      {body: {},                  nextCalled: false, jsonCalled: true,  statusCalled: true},
      {body: {array: [1,2,3]},    nextCalled: false, jsonCalled: true,  statusCalled: true},
      {body: {key: "value"},      nextCalled: false, jsonCalled: true,  statusCalled: true},
      {body: {list: [1,2,3,4,5]}, nextCalled: false, jsonCalled: true,  statusCalled: true},
      {body: {list: ["a", "s"]},  nextCalled: true,  jsonCalled: false, statusCalled: false},
    ]

    testParams.forEach(t => {
      let res = httpMocks.createResponse()
      let req = httpMocks.createRequest(      {
        method: 'GET',
        url: '/subreddits',
        body: t.body
      });

      let jsonSpy = sinon.spy(res, 'json')
      let statusSpy = sinon.spy(res, 'status')
      let nextSpy = sinon.spy()

      validateJSONBody(schema)(req, res, nextSpy)

      assert.equal(nextSpy.called, t.nextCalled);
      assert.equal(jsonSpy.called, t.jsonCalled);
      assert.equal(statusSpy.called, t.statusCalled);
      if (t.statusCalled) {
        assert.equal(statusSpy.args[0][0], 400)
      }
    })
  })
})
