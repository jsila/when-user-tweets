const forEach = require('lodash/forEach')

const routes = {
  '/tweets': './tweets'
}

module.exports.useRoutes = app => {
  forEach(routes, (module, endpoint) => {
    app.use(endpoint, require(module))
  })
}
