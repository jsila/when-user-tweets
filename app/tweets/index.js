const express = require('express')

const validateJSONBody = require('../utils/middlewares/validateJSONBody')
const saveSchema = require('./schemas/save')

const ctrl = require('./controller')

let router = express.Router()
router.post('/', validateJSONBody(saveSchema), ctrl.save);

module.exports = router
