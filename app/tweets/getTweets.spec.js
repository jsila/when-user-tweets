const proxyquire = require('proxyquire')
const assert = require('assert')
const sinon = require('sinon')

let twitter = require('twitter')
let config = {
  twitter: {
    params: {},
    credentials: {}
  },
  sentry: {}
}

let getTweets = proxyquire('./getTweets', { twitter, '../../config': config })

describe('getTweets', () => {
  it('is a promise ', sinon.test(function() {
    this.stub(twitter.prototype, 'get', () => 1)
    assert(getTweets().then);
  }))

  it('twitter client accepts screen_name', sinon.test(function() {
    let screenName = 'jernejsila'

    let twitterGetStub = this.stub(twitter.prototype, 'get', () => 1)

    getTweets(screenName)

    assert.equal(config.twitter.params.screen_name, screenName)
    assert.deepEqual(twitterGetStub.args[0][1], config.twitter.params)
  }))
})
