const getTweets = require('./getTweets')

module.exports.save = (req, res, next) => {
  getTweets(req.body.screen_name).then(tweets => {
    tweetsTimes = tweets.map(t => t.created_at);
    res.json(tweetsTimes);
  }).catch(err => {
    res.sendStatus(400)
  });
}
