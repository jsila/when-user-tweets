const proxyquire = require('proxyquire')
const assert = require('assert')
const sinon = require('sinon')
const httpMock = require('node-mocks-http')
var sinonStubPromise = require('sinon-stub-promise');
sinonStubPromise(sinon);

let getTweetsStub = sinon.stub().returnsPromise()
let ctrl = proxyquire('./controller', { './getTweets': getTweetsStub })

describe('controller', () => {

  it('returns tweets\' times in json', sinon.test(function() {
    let tweets = [{a:'a', created_at:'time a'},{b:'b', created_at:'time b'}]
    let screenName = 'jernejsila'

    let req = httpMock.createRequest({
      body: {
        screen_name: screenName
      }
    })
    let res = httpMock.createResponse()
    let jsonSpy = this.spy(res, 'json');

    getTweetsStub.resolves(tweets)

    ctrl.save(req, res)

    assert(jsonSpy.called);
    assert(getTweetsStub.calledWith(screenName))
    assert(jsonSpy.calledWith(['time a', 'time b']));
  }))
})
