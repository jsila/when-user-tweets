const Twitter = require('twitter');
let {params, credentials} = require('../../config').twitter

let client = new Twitter(credentials);

const getTweets = screen_name => new Promise((resolve, reject) => {
  params.screen_name = screen_name

  client.get('statuses/user_timeline', params, (err, tweets) => {
    if (err) {
      reject(err);
    } else {
      resolve(tweets);
    }
  });
});

module.exports = getTweets;
