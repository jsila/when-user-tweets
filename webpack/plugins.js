const webpack = require('webpack');
const {resolve} = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');

let production = [
  new webpack.optimize.DedupePlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"'
    }
  }),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      screw_ie8: true,
      warnings: false
    }
  }),
  new webpack.optimize.OccurenceOrderPlugin(),
  new ExtractTextPlugin('main.[hash].css')
];

let development = [
  new webpack.HotModuleReplacementPlugin(),
];

let common = [
  new webpack.NoErrorsPlugin(),
  new HtmlWebpackPlugin({
    template: resolve('src', 'index.html')
  }),
  new CleanPlugin(['public'], {
    root: resolve(),
    verbose: false,
    dry: true,
  })
];

module.exports = isProd => [
  ...common,
  ...(isProd ? production : development)
]
