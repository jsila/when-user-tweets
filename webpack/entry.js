const {resolve} = require('path');

let production = [];

let development = [
  'webpack-hot-middleware/client',
];

let common = [
  resolve('src', 'main.js'),
  resolve('src', 'main.scss'),
]

module.exports = isProd => [
  ...common,
  ...(isProd ? production : development)
]
