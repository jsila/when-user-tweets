const request = require('supertest')

const app = require('../../')

describe('POST /tweets', () => {
  describe('returns 400, when request body', () => {
    let testCases = [{
      description: 'is empty',
      body: undefined
    }, {
      description: 'doesn\'t satisfy json schema',
      body: {test: 'wrong'}
    }, {
      description: 'satisfy json schema, but have non-existent twitter username',
      body: {screen_name: 'asaklobasaJEDNA'}
    }]

    testCases.forEach(c => {
      it(c.description, done => {
        request(app)
          .post('/tweets')
          .send(c.body)
          .expect(400, done)
      })
    })
  })

  it('returns data if request body satisfies json schema and has valid  twitter username', done => {
    request(app)
      .post('/tweets')
      .send({screen_name: 'jernejsila'})
      .expect(200, done)
  })
})

