const isProduction = process.env.NODE_ENV == 'production'
const isTesting = process.env.NODE_ENV == 'testing'
const isDevelopment = !isTesting && !isProduction

const PORT = process.env.PORT || 8080

const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const raven = require('raven')

const {useRoutes} = require('./app')
const config = require('./config')

const sentryUrl = `https://${config.sentry.key}@sentry.io/${config.sentry.app}`

let app = express()
module.exports = app

if (isDevelopment) {
  app.use(express.static('public'))
  require('./webpack/applyMiddlewares')(app)
}

if (isProduction) {
  app.use(raven.middleware.express.requestHandler(sentryUrl))
}

app.use(bodyParser.json())
app.use(helmet())

useRoutes(app)

if (isProduction) {
  app.use(raven.middleware.express.errorHandler(sentryUrl))
  app.use((err, req, res, next) => {
    res.sendStatus(500)
  })
}

if (!isTesting) {
  let server = app.listen(PORT, () => {
    console.log('Running on http://localhost:%s', server.address().port)
  })
}
